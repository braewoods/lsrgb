// Copyright (c) 2024 James Buren
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "local.h"
#include <ini.h>

static bool parse_int(const char *key, const char *val, int low, int high, int *out) {
    long n;

    if (*val == '\0') {
        output("%s must be a non-empty string", key);
        return false;
    }

    if (!strtos(val, 10, &n)) {
        output("%s must be an integer", key);
        return false;
    }

    if (!ranges(n, low, high)) {
        output("%s must be between %d and %d", key, low, high);
        return false;
    }

    *out = n;
    return true;
}

static bool parse_str(const char *key, const char *val, char *buf, size_t size) {
    if (*val == '\0') {
        output("%s must be a non-empty string", key);
        return false;
    }

    strscpy(buf, size, val);
    return true;
}

static bool parse_rgb24(const char *key, const char *val, int *out) {
    long n;
    const int low = 0x000000;
    const int high = 0xffffff;

    if (*val == '\0') {
        output("%s must be a non-empty string", key);
        return false;
    }

    if (!strtos(val, 16, &n)) {
        output("%s must be a hexadecimal string", key);
        return false;
    }

    if (!ranges(n, low, high)) {
        output("%s must be between %06x and %06x", key, low, high);
        return false;
    }

    *out = n;
    return true;
}

static bool parse_key(const char *key, const char *val, key_color kcs[static KEY_COLORS_MAX], size_t *kcs_len) {
    char buf[256];
    char *tmp;
    char *key2;
    char *val2;
    long n;
    const int low = 0x000000;
    const int high = 0xffffff;
    key_color *kc;

    if (*val == '\0') {
        output("%s must be a non-empty string", key);
        return false;
    }

    strscpy(buf, sizeof(buf), val);
    tmp = buf;

    key2 = strsep(&tmp, ":");
    val2 = strsep(&tmp, ":");

    if (key2 == NULL || val2 == NULL || tmp != NULL) {
        output("%s must be a colon separated pair of strings", key);
        return false;
    }

    if (*key2 == '\0') {
        output("first field of %s must be a non-empty string", key);
        return false;
    }

    if (*val2 == '\0') {
        output("second field of %s must be a non-empty string", key);
        return false;
    }

    if (!strtos(val2, 16, &n)) {
        output("second field of %s must be a hexadecimal string", key);
        return false;
    }

    if (!ranges(n, low, high)) {
        output("second field of %s must be between %06x and %06x", key, low, high);
        return false;
    }

    if (*kcs_len == KEY_COLORS_MAX) {
        output("cannot handle any more key colors");
        return false;
    }

    kc = &kcs[(*kcs_len)++];
    strscpy(kc->key, sizeof(kc->key), key2);
    kc->color = n;

    return true;
}

static bool parse_bool(const char *key, const char *val, int *out) {
    int b;

    if (strcmp(val, "yes") == 0) {
        b = true;
    } else if (strcmp(val, "no") == 0) {
        b = false;
    } else {
        output("%s must be a boolean value", key);
        return false;
    }

    *out = b;
    return true;
}

static int callback(void *ud, const char *sect, const char *key, const char *val) {
    (void) ud;

    if (*sect != '\0') {
        output("named sections are not supported");
        return false;
    }

    if (key == NULL && val == NULL)
        return true;

    if (val == NULL) {
        output("fields must have a value");
        return false;
    }

    if (strcmp(key, "speed") == 0)
        return parse_int(key, val, 1, 100, &ms.speed);

    if (strcmp(key, "mode") == 0)
        return parse_str(key, val, ms.mode, sizeof(ms.mode));

    if (strcmp(key, "filter") == 0)
        return parse_rgb24(key, val, &ms.filter);

    if (strcmp(key, "brightness") == 0)
        return parse_int(key, val, 0, 100, &ms.brightness);

    if (strcmp(key, "deflection") == 0)
        return parse_int(key, val, 0, 100, &ms.deflection);

    if (strcmp(key, "key") == 0)
        return parse_key(key, val, ms.key_colors, &ms.key_colors_len);

    if (strcmp(key, "default_key_colors") == 0)
        return parse_bool(key, val, &ms.default_key_colors);

    output("encountered unknown field: %s", key);
    return false;
}

bool settings_parse(const char *path) {
    char err[ERR_MAX];
    int res;

    if (path == NULL)
        return true;

    if (!change_privileges(PRIVILEGES_DISCARD))
        return false;

    res = ini_parse(path, callback, NULL);

    if (!change_privileges(PRIVILEGES_RESTORE))
        return false;

    if (res < 0) {
        if (res == -1)
            output("%s: %s: %s", "fopen", errnotostr(err), path);
        else if (res == -2)
            output("%s: %s: %s", "malloc", errnotostr(err), path);
        else
            output("unknown error while parsing %s", path);
        return false;
    }

    if (res > 0) {
        output("error while parsing line %d: %s", res, path);
        return false;
    }

    return true;
}
