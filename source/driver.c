// Copyright (c) 2024 James Buren
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "local.h"

enum {
    MODEL_TIMEOUT = 100,
    MODEL_RETRIES = 5,
};

static bool driver_v1_get_model(device *dev, char model[static DRIVER_V1_IRS + 1]) {
    unsigned char obuf[DRIVER_V1_ORS + 1];
    unsigned char ibuf[DRIVER_V1_IRS + 1];
    ssize_t n;

    obuf[0] = 0x00;
    obuf[1] = 0xfe;
    obuf[2] = 0xff;
    memset(obuf + 3, 0x00, sizeof(obuf) - 3);

    n = device_write(dev, obuf, sizeof(obuf));
    if (n != sizeof(obuf))
        return false;

    n = device_read(dev, ibuf, sizeof(ibuf), MODEL_TIMEOUT);
    if (n != sizeof(ibuf))
        return false;

    strscpy(model, DRIVER_V1_IRS + 1, (char *) (ibuf + 1));
    return true;
}

static const driver *driver_v1_find_model(const char *model) {
    typedef struct table_entry {
        const char *model;
        const driver *driver;
    } table_entry;
    static const table_entry table[] = {
        {"Light&Salt_keyboard1", &driver_keyboard_v1},
    };
    static const int table_len = array_len(table);

    for (int i = 0; i < table_len; i++) {
        const table_entry *dte = &table[i];

        if (strcmp(dte->model, model) != 0)
            continue;

        return dte->driver;
    }

    return NULL;
}

static const driver *driver_v1_find(device *dev) {
    char model[DRIVER_V1_IRS + 1];
    int retry;

    // NetBSD / OpenBSD may require multiple attempts due to kernel bugs
    for (retry = 0; retry < MODEL_RETRIES; retry++)
        if (driver_v1_get_model(dev, model))
            break;

    return (retry == MODEL_RETRIES) ? NULL : driver_v1_find_model(model);
}

const driver *driver_find(const device_table_entry *dte, device *dev) {
    if (dte->irs == DRIVER_V1_IRS && dte->ors == DRIVER_V1_ORS && dte->frs == DRIVER_V1_FRS)
        return driver_v1_find(dev);

    return NULL;
}
