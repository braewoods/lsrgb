// Copyright (c) 2024 James Buren
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "local.h"

typedef struct key_table_entry {
    const char *key1;
    const char *key2;
    int row;
    int column;
    int matrix[4];
} key_table_entry;

static const key_table_entry key_table[] = {
    {        "escape",           "esc", 0, 0, {1, 1, 1, 1}},
    {            "f1",            NULL, 0, 1, {1, 1, 1, 1}},
    {            "f2",            NULL, 0, 2, {1, 1, 1, 1}},
    {            "f3",            NULL, 0, 3, {1, 1, 1, 1}},
    {            "f4",            NULL, 0, 4, {1, 1, 1, 1}},
    {            "f5",            NULL, 0, 5, {1, 1, 1, 1}},
    {            "f6",            NULL, 0, 6, {1, 1, 1, 1}},
    {            "f7",            NULL, 0, 7, {1, 1, 1, 1}},
    {            "f8",            NULL, 0, 8, {1, 1, 1, 1}},
    {            "f9",            NULL, 0, 9, {1, 1, 1, 1}},
    {           "f10",            NULL, 1, 0, {1, 1, 1, 1}},
    {           "f11",            NULL, 1, 1, {1, 1, 1, 1}},
    {           "f12",            NULL, 1, 2, {1, 1, 1, 1}},
    {            NULL,            NULL, 1, 3, {0, 0, 0, 0}},
    {            NULL,            NULL, 1, 4, {0, 0, 0, 0}},
    {      "backtick",         "tilde", 1, 5, {1, 2, 1, 2}},
    {             "1",   "exclamation", 1, 6, {1, 2, 1, 2}},
    {             "2",            "at", 1, 7, {1, 2, 1, 2}},
    {             "3",         "pound", 1, 8, {1, 2, 1, 2}},
    {             "4",        "dollar", 1, 9, {1, 2, 1, 2}},
    {             "5",       "percent", 2, 0, {1, 2, 1, 2}},
    {             "6",    "circumflex", 2, 1, {1, 2, 1, 2}},
    {             "7",     "ampersand", 2, 2, {1, 2, 1, 2}},
    {             "8",      "asterisk", 2, 3, {1, 2, 1, 2}},
    {             "9", "parenthesis_l", 2, 4, {1, 2, 1, 2}},
    {             "0", "parenthesis_r", 2, 5, {1, 2, 1, 2}},
    {         "minus",    "underscore", 2, 6, {1, 2, 1, 2}},
    {         "equal",          "plus", 2, 7, {1, 2, 1, 2}},
    {     "backspace",            "bs", 2, 8, {1, 1, 1, 1}},
    {        "delete",           "del", 2, 9, {1, 1, 1, 1}},
    {           "tab",            NULL, 3, 0, {1, 1, 1, 1}},
    {             "q",            NULL, 3, 1, {1, 2, 2, 1}},
    {             "w",            NULL, 3, 2, {1, 2, 2, 1}},
    {             "e",            NULL, 3, 3, {1, 2, 2, 1}},
    {             "r",            NULL, 3, 4, {1, 2, 2, 1}},
    {             "t",            NULL, 3, 5, {1, 2, 2, 1}},
    {             "y",            NULL, 3, 6, {1, 2, 2, 1}},
    {             "u",            NULL, 3, 7, {1, 2, 2, 1}},
    {             "i",            NULL, 3, 8, {1, 2, 2, 1}},
    {             "o",            NULL, 3, 9, {1, 2, 2, 1}},
    {             "p",            NULL, 4, 0, {1, 2, 2, 1}},
    {"square_brace_l", "curly_brace_l", 4, 1, {1, 2, 1, 2}},
    {"square_brace_r", "curly_brace_r", 4, 2, {1, 2, 1, 2}},
    {     "backslash",   "verticalbar", 4, 3, {1, 2, 1, 2}},
    {         "print",           "prt", 4, 4, {1, 1, 1, 1}},
    {      "capslock",          "caps", 4, 5, {1, 1, 1, 1}},
    {             "a",            NULL, 4, 6, {1, 2, 2, 1}},
    {             "s",            NULL, 4, 7, {1, 2, 2, 1}},
    {             "d",            NULL, 4, 8, {1, 2, 2, 1}},
    {             "f",            NULL, 4, 9, {1, 2, 2, 1}},
    {             "g",            NULL, 5, 0, {1, 2, 2, 1}},
    {             "h",            NULL, 5, 1, {1, 2, 2, 1}},
    {             "j",            NULL, 5, 2, {1, 2, 2, 1}},
    {             "k",            NULL, 5, 3, {1, 2, 2, 1}},
    {             "l",            NULL, 5, 4, {1, 2, 2, 1}},
    {     "semicolon",         "colon", 5, 5, {1, 2, 1, 2}},
    {    "apostrophe",     "quotation", 5, 6, {1, 2, 1, 2}},
    {        "return",         "enter", 5, 7, {1, 1, 1, 1}},
    {            NULL,            NULL, 5, 8, {0, 0, 0, 0}},
    {            NULL,            NULL, 5, 9, {0, 0, 0, 0}},
    {       "shift_l",       "l_shift", 6, 0, {1, 1, 1, 1}},
    {             "z",            NULL, 6, 1, {1, 2, 2, 1}},
    {             "x",            NULL, 6, 2, {1, 2, 2, 1}},
    {             "c",            NULL, 6, 3, {1, 2, 2, 1}},
    {             "v",            NULL, 6, 4, {1, 2, 2, 1}},
    {             "b",            NULL, 6, 5, {1, 2, 2, 1}},
    {             "n",            NULL, 6, 6, {1, 2, 2, 1}},
    {             "m",            NULL, 6, 7, {1, 2, 2, 1}},
    {         "comma",          "less", 6, 8, {1, 2, 1, 2}},
    {        "period",       "greater", 6, 9, {1, 2, 1, 2}},
    {         "slash",      "question", 7, 0, {1, 2, 1, 2}},
    {       "shift_r",       "r_shift", 7, 1, {1, 1, 1, 1}},
    {            NULL,            NULL, 7, 2, {0, 0, 0, 0}},
    {            "up",            NULL, 7, 3, {1, 1, 1, 1}},
    {            NULL,            NULL, 7, 4, {0, 0, 0, 0}},
    {        "ctrl_l",        "l_ctrl", 7, 5, {1, 1, 1, 1}},
    {       "windows",         "super", 7, 6, {1, 1, 1, 1}},
    {         "alt_l",         "l_alt", 7, 7, {1, 1, 1, 1}},
    {        "space1",            NULL, 7, 8, {1, 1, 1, 1}},
    {        "space2",            NULL, 7, 9, {1, 1, 1, 1}},
    {        "space3",            NULL, 8, 0, {1, 1, 1, 1}},
    {        "space4",            NULL, 8, 1, {1, 1, 1, 1}},
    {        "space5",            NULL, 8, 2, {1, 1, 1, 1}},
    {         "alt_r",         "r_alt", 8, 3, {1, 1, 1, 1}},
    {      "function",            "fn", 8, 4, {1, 1, 1, 1}},
    {        "ctrl_r",        "r_ctrl", 8, 5, {1, 1, 1, 1}},
    {            NULL,            NULL, 8, 6, {0, 0, 0, 0}},
    {          "left",            NULL, 8, 7, {1, 1, 1, 1}},
    {          "down",            NULL, 8, 8, {1, 1, 1, 1}},
    {         "right",            NULL, 8, 9, {1, 1, 1, 1}},
};
static const int key_table_len = array_len(key_table);

static ssize_t send_packet(device *dev, const unsigned char *buf, size_t size) {
    char err[ERR_MAX];
    ssize_t n = device_write(dev, buf, size);

    if (n == -1)
        output("%s: %s", __func__, errnotostr(err));
    else if (n != (ssize_t) size)
        output("%s: %s", __func__, "short packet");

    return n;
}

static const unsigned char *find_mode(const char *key) {
    typedef struct table_entry {
        const char *key;
        unsigned char mode[2];
    } table_entry;
    static const table_entry table[] = {
        {     "static", {0x00, 0x00}},
        {"reactive-1a", {0x01, 0x00}},
        {"reactive-1b", {0x02, 0x00}},
        {"reactive-2a", {0x04, 0x00}},
        {"reactive-2b", {0x05, 0x00}},
        {"reactive-3a", {0x07, 0x00}},
        {"reactive-3b", {0x08, 0x00}},
        {"reactive-4a", {0x0a, 0x00}},
        {"reactive-4b", {0x0b, 0x00}},
        { "reactive-5", {0x0d, 0x00}},
        { "reactive-6", {0x0e, 0x00}},
        { "reactive-7", {0x0f, 0x00}},
        { "reactive-8", {0x11, 0x00}},
        { "dynamic-1n", {0x33, 0x01}},
        { "dynamic-1r", {0x33, 0x02}},
        { "dynamic-2n", {0x34, 0x01}},
        { "dynamic-2r", {0x34, 0x02}},
        { "dynamic-3n", {0x35, 0x01}},
        { "dynamic-3r", {0x35, 0x02}},
        { "dynamic-4n", {0x36, 0x01}},
        { "dynamic-4r", {0x36, 0x02}},
        { "dynamic-5n", {0x37, 0x01}},
        { "dynamic-5r", {0x37, 0x02}},
        { "dynamic-6n", {0x38, 0x01}},
        { "dynamic-6r", {0x38, 0x02}},
        { "dynamic-7n", {0x39, 0x01}},
        { "dynamic-7r", {0x39, 0x02}},
        { "dynamic-8n", {0x3a, 0x01}},
        { "dynamic-8r", {0x3a, 0x02}},
        { "dynamic-9n", {0x3b, 0x01}},
        { "dynamic-9r", {0x3b, 0x02}},
    };
    static const int table_len = array_len(table);

    for (int i = 0; i < table_len; i++) {
        const table_entry *mt = &table[i];

        if (strcmp(mt->key, key) != 0)
            continue;

        return mt->mode;
    }

    return NULL;
}

static const key_table_entry *find_key(const char *key) {
    for (int i = 0; i < key_table_len; i++) {
        const key_table_entry *kte = &key_table[i];
        const char *key1 = kte->key1;
        const char *key2 = kte->key2;

        if (key1 != NULL && strcmp(key, key1) == 0)
            return kte;

        if (key2 != NULL && strcmp(key, key2) == 0)
            return kte;
    }

    return NULL;
}

static int color_deflection(int color, int deflection) {
    return (color + ((255 - color) * deflection / 100));
}

static void set_key_color(rgb888 (*key_colors)[4][9][10], const key_table_entry *kte, rgb888 in, int deflection) {
    for (int i = 0; i < 4; i++) {
        int n = kte->matrix[i];
        rgb888 *out = &(*key_colors)[i][kte->row][kte->column];

        if (n == 0) {
            out->red = 0x00;
            out->green = 0x00;
            out->blue = 0x00;
        } else if (n == 1) {
            out->red = in.red;
            out->green = in.green;
            out->blue = in.blue;
        } else if (n == 2) {
            out->red = color_deflection(in.red, deflection);
            out->green = color_deflection(in.green, deflection);
            out->blue = color_deflection(in.blue, deflection);
        }
    }
}

static bool send_mode(device *dev, const unsigned char *mode, int speed) {
    unsigned char obuf[DRIVER_V1_ORS + 1];

    obuf[0] = 0x00;
    obuf[1] = 0xfe;
    obuf[2] = 0x03;
    obuf[3] = mode[0];
    obuf[4] = mode[1];
    obuf[5] = 50 * speed / 100;
    memset(obuf + 6, 0x00, sizeof(obuf) - 6);

    return (send_packet(dev, obuf, sizeof(obuf)) == sizeof(obuf));
}

static bool send_filter(device *dev, rgb888 filter) {
    unsigned char obuf[DRIVER_V1_ORS + 1];

    obuf[0] = 0x00;
    obuf[1] = 0xfe;
    obuf[2] = 0x04;
    obuf[3] = filter.red;
    obuf[4] = filter.green;
    obuf[5] = filter.blue;
    memset(obuf + 6, 0x00, sizeof(obuf) - 6);

    return (send_packet(dev, obuf, sizeof(obuf)) == sizeof(obuf));
}

static bool send_brightness(device *dev, int brightness) {
    unsigned char obuf[DRIVER_V1_ORS + 1];

    obuf[0] = 0x00;
    obuf[1] = 0xfe;
    obuf[2] = 0x05;
    obuf[3] = 255 * brightness / 100;
    memset(obuf + 4, 0x00, sizeof(obuf) - 4);

    return (send_packet(dev, obuf, sizeof(obuf)) == sizeof(obuf));
}

static void set_default_key_colors(rgb888 (*key_colors)[4][9][10], int deflection) {
    static const rgb888 key_color = {0x50, 0x50, 0x46};

    for (int i = 0; i < key_table_len; i++)
        set_key_color(key_colors, &key_table[i], key_color, deflection);
}

static bool set_key_colors(rgb888 (*key_colors)[4][9][10], const key_color *kcs, size_t kcs_len, int deflection) {
    for (size_t i = 0; i < kcs_len; i++) {
        const key_color *kc = &kcs[i];
        const key_table_entry *kte = find_key(kc->key);

        if (kte == NULL) {
            output("unknown key: %s", kc->key);
            return false;
        }

        set_key_color(key_colors, kte, rgb24torgb888(kc->color), deflection);
    }

    return true;
}

static bool send_key_colors(device *dev, rgb888 (*key_colors)[4][9][10]) {
    unsigned char obuf[DRIVER_V1_ORS + 1];

    obuf[0] = 0x00;

    for (int i = 0; i < 4; i++) {
        obuf[1] = 0x03 + i;

        for (int j = 0; j < 9; j++) {
            obuf[2] = 0x01 + j * 10;

            memcpy(obuf + 3, (*key_colors)[i][j], sizeof(obuf) - 3);

            if (send_packet(dev, obuf, sizeof(obuf)) != sizeof(obuf))
                return false;
        }
    }

    return true;
}

static bool apply_key_colors(device *dev) {
    unsigned char obuf[DRIVER_V1_ORS + 1];

    obuf[0] = 0x00;
    obuf[1] = 0xfe;
    obuf[2] = 0x01;
    obuf[3] = 0x03;
    obuf[4] = 0x04;
    obuf[5] = 0x05;
    obuf[6] = 0x06;
    memset(obuf + 7, 0x00, sizeof(obuf) - 7);

    return (send_packet(dev, obuf, sizeof(obuf)) == sizeof(obuf));
}

static bool save_key_colors(device *dev) {
    unsigned char obuf[DRIVER_V1_ORS + 1];

    obuf[0] = 0x00;
    obuf[1] = 0xfe;
    obuf[2] = 0xf0;
    obuf[3] = 0x03;
    obuf[4] = 0x04;
    obuf[5] = 0x05;
    obuf[6] = 0x06;
    memset(obuf + 7, 0x00, sizeof(obuf) - 7);

    return (send_packet(dev, obuf, sizeof(obuf)) == sizeof(obuf));
}

static bool update_mode(device *dev, const char *key, int speed) {
    const unsigned char *mode;

    if (*key == '\0')
        return true;

    if (speed < 0)
        speed = 50;

    mode = find_mode(key);
    if (mode == NULL) {
        output("unknown mode: %s", key);
        return false;
    }

    return send_mode(dev, mode, speed);
}

static bool update_filter(device *dev, int filter) {
    if (filter < 0)
        return true;

    return send_filter(dev, rgb24torgb888(filter));
}

static bool update_brightness(device *dev, int brightness) {
    if (brightness < 0)
        return true;

    return send_brightness(dev, brightness);
}

static bool update_key_colors(device *dev, const key_color *kcs, size_t kcs_len, int default_key_colors, int deflection) {
    rgb888 key_colors[4][9][10];

    if (kcs_len == 0 && !default_key_colors)
        return true;

    if (deflection < 0)
        deflection = 50;

    set_default_key_colors(&key_colors, deflection);

    if (!set_key_colors(&key_colors, kcs, kcs_len, deflection))
        return false;

    if (!send_key_colors(dev, &key_colors))
        return false;

    if (!apply_key_colors(dev))
        return false;

    if (!save_key_colors(dev))
        return false;

    return true;
}

const driver driver_keyboard_v1 = {
    update_mode,
    update_filter,
    update_brightness,
    update_key_colors,
};
