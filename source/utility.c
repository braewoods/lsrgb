// Copyright (c) 2024 James Buren
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "local.h"

static const char digits[] = "0123456789";

void *dim_real(size_t size, bool zero, const char *type) {
    char err[ERR_MAX];
    void *ptr;

    ptr = malloc(size);
    if (ptr == NULL) {
        output("%s: %s: %s", "dim", errnotostr(err), type);
        return NULL;
    }

    if (zero)
        memset(ptr, 0x00, size);

    return ptr;
}

void *redim_real(void *ptr, size_t size, const char *type) {
    char err[ERR_MAX];
    void *ptr2;

    ptr2 = realloc(ptr, size);
    if (ptr2 == NULL) {
        output("%s: %s: %s", "redim", errnotostr(err), type);
        return NULL;
    }

    return ptr2;
}

bool strbuilder(char * restrict dst, size_t size, char const * restrict * restrict arr) {
    const char *src;

    *dst = '\0';

    while ((src = *arr) != NULL) {
        char *end;
        size_t len;

        end = memccpy(dst, src, '\0', size);
        if (end == NULL) {
            dst[size - 1] = '\0';
            return false;
        }

        len = end - dst - 1;
        dst += len;
        size -= len;
        arr++;
    }

    return true;
}

bool strscpy(char * restrict dst, size_t size, char const * restrict src) {
    char *end = memccpy(dst, src, '\0', size);

    if (end == NULL)
        dst[size - 1] = '\0';

    return (end != NULL);
}

char *stostr(char buf[static DIGITS_MAX], long num) {
    char *s = buf + (DIGITS_MAX - 1);
    long n = num;

    *s-- = '\0';

    do {
        *s-- = digits[labs(n % 10)];
        n /= 10;
    } while (n != 0);

    if (num < 0)
        *s-- = '-';

    return (s + 1);
}

void format_usb_ports(char *buf, size_t size, int bus, const int ports[static USB_PORTS_MAX], size_t len, int cn, int in) {
    const char *fields[3 + 2 * (USB_PORTS_MAX - 1) + 5];
    char nums[1 + USB_PORTS_MAX + 2][DIGITS_MAX];
    size_t i = 0;
    size_t j = 0;
    size_t k = 0;

    fields[i++] = stostr(nums[j++], bus);
    fields[i++] = "-";
    fields[i++] = stostr(nums[j++], ports[k++]);

    while (k < len) {
        fields[i++] = ".";
        fields[i++] = stostr(nums[j++], ports[k++]);
    }

    fields[i++] = ":";
    fields[i++] = stostr(nums[j++], cn);
    fields[i++] = ".";
    fields[i++] = stostr(nums[j++], in);
    fields[i++] = NULL;

    strbuilder(buf, size, fields);
}

bool strtos(const char *s, int b, long *n) {
    char *e;

    errno = 0;
    *n = strtol(s, &e, b);

    return (errno == 0 && s != e && *e == '\0');
}

size_t digspn(const char *s) {
    return strspn(s, digits);
}

size_t digcspn(const char *s) {
    return strcspn(s, digits);
}

const char *errnotostr(char buf[static ERR_MAX]) {
    return errtostr(buf, errno);
}

const char *errtostr(char buf[static ERR_MAX], int err) {
#if defined(HAVE_MS_STRERROR_S)
    strerror_s(buf, ERR_MAX, err);
    return buf;
#elif defined(HAVE_GNU_STRERROR_R)
    return strerror_r(err, buf, ERR_MAX);
#elif defined(HAVE_POSIX_STRERROR_R)
    int res = strerror_r(err, buf, ERR_MAX);

    if (res == EINVAL)
        strscpy(buf, ERR_MAX, "Unknown error");
    else if (res == ERANGE)
        strscpy(buf, ERR_MAX, "Error truncated");

    return buf;
#else
#error "Unsupported Platform"
#endif
}

bool is_hidraw(const char *s) {
    size_t n;

    if (strncmp(s, "hidraw", 6) != 0)
        return false;

    n = digspn(s += 6);
    if (n == 0)
        return false;

    s += n;
    return (*s == '\0');
}

bool is_uhid(const char *s) {
    size_t n;

    if (strncmp(s, "uhid", 4) != 0)
        return false;

    n = digspn(s += 4);
    if (n == 0)
        return false;

    s += n;
    return (*s == '\0');
}

bool is_uhub(const char *s) {
    size_t n;

    if (strncmp(s, "uhub", 4) != 0)
        return false;

    n = digspn(s += 4);
    if (n == 0)
        return false;

    s += n;
    return (*s == '\0');
}

bool change_privileges(int act) {
#if defined(__MINGW32__)
    (void) act;
    return true;
#else
    static uid_t initial_uid;
    static uid_t initial_euid;
    static bool initialized;
    char err[ERR_MAX];
    uid_t uid;

    if (!initialized) {
        initial_uid = getuid();
        initial_euid = geteuid();
        initialized = true;
    }

    if (act == PRIVILEGES_DISCARD) {
        uid = initial_uid;
    } else if (act == PRIVILEGES_RESTORE) {
        uid = initial_euid;
    } else {
        output("%s: %s", __func__, errtostr(err, EINVAL));
        return false;
    }

    if (seteuid(uid) < 0) {
        output("%s: %s", "seteuid", errnotostr(err));
        return false;
    }

    return true;
#endif
}

void output(const char *fmt, ...) {
    va_list args;

    if (ms.program_name != NULL && *ms.program_name != '\0') {
        fputs(ms.program_name, stdout);
        fputs(": ", stdout);
    }

    va_start(args, fmt);
    vfprintf(stdout, fmt, args);
    va_end(args);

    fputc('\n', stdout);
}
