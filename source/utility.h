// Copyright (c) 2024 James Buren
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#pragma once

#define strbuild(A,B,...) strbuilder(A,B,(const char * []){__VA_ARGS__,NULL})
#define dim(T,N) ((T*)dim_real(sizeof(T[N]),false,#T))
#define dim0(T,N) ((T*)dim_real(sizeof(T[N]),true,#T))
#define redim(P,T,N) ((T*)redim_real(P,sizeof(T[N]),#T))
#define array_len(A) (sizeof(A)/sizeof(*A))

enum {
    DIGITS_MAX = 20 + 1,
    USB_PORTS_MAX = 32,
    ERR_MAX = 256,
};

enum {
    PRIVILEGES_DISCARD,
    PRIVILEGES_RESTORE,
};

typedef struct rgb888 {
    unsigned char red;
    unsigned char green;
    unsigned char blue;
} rgb888;

static inline long mins(long a, long b) {
    return (a < b) ? a : b;
}

static inline long maxs(long a, long b) {
    return (a > b) ? a : b;
}

static inline long clamps(long n, long l, long h) {
    return (n < l) ? l : (n > h) ? h : n;
}

static inline bool ranges(long n, long l, long h) {
    return (n >= l && n <= h);
}

static inline rgb888 rgb24torgb888(int in) {
    rgb888 out;
    out.red   = (in & (0xff << 16)) >> 16;
    out.green = (in & (0xff <<  8)) >>  8;
    out.blue  = (in & (0xff <<  0)) >>  0;
    return out;
}

void *dim_real(size_t, bool, const char *);
void *redim_real(void *, size_t, const char *);
bool strbuilder(char * restrict, size_t, char const * restrict * restrict);
bool strscpy(char * restrict, size_t, char const * restrict);
char *stostr(char [static DIGITS_MAX], long);
void format_usb_ports(char *, size_t, int, const int [static USB_PORTS_MAX], size_t, int, int);
bool strtos(const char *, int, long *);
size_t digspn(const char *);
size_t digcspn(const char *);
const char *errnotostr(char buf[static ERR_MAX]);
const char *errtostr(char [static ERR_MAX], int);
bool is_hidraw(const char *);
bool is_uhid(const char *);
bool is_uhub(const char *);
bool change_privileges(int);
void output(const char *, ...) __attribute__((format(printf, 1, 2)));
