// Copyright (c) 2024 James Buren
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "local.h"

main_state ms = {
    NULL,
    NULL,
    NULL,
    {{"", 0}},
    0,
    0,
    -1,
    -1,
    -1,
    -1,
    -1,
    "",
};

static void program_fini(void) {

}

static bool program_init(const char *argv0) {
    const char *s;

#if defined(__MINGW32__)
    s = strrchr(argv0, '\\');
    if (s == NULL)
        s = strrchr(argv0, '/');
#else
    s = strrchr(argv0, '/');
#endif

    ms.program_name = (s != NULL) ? (s + 1) : argv0;

    tzset();

    if (setlocale(LC_ALL, "") == NULL) {
        return false;
    }

    if (atexit(program_fini) != 0) {
        output("%s: %s", "atexit", "registration failed");
        return false;
    }

    return true;
}

static bool parse_int(int opt, const char *arg, int low, int high, int *out) {
    long n;

    if (*arg == '\0') {
        output("argument for option '%c' must be non-empty", opt);
        return false;
    }

    if (!strtos(arg, 10, &n)) {
        output("argument for option '%c' must be an integer", opt);
        return false;
    }

    if (!ranges(n, low, high)) {
        output("argument for option '%c' must be between %d and %d", opt, low, high);
        return false;
    }

    *out = n;
    return true;
}

static bool parse_str(int opt, const char *arg, const char **out) {
    if (*arg == '\0') {
        output("argument for option '%c' must be non-empty", opt);
        return false;
    }

    *out = arg;
    return true;
}

static bool parse_str2(int opt, const char *arg, char *buf, size_t size) {
    if (*arg == '\0') {
        output("argument for option '%c' must be non-empty", opt);
        return false;
    }

    strscpy(buf, size, arg);
    return true;
}

static bool parse_rgb24(int opt, const char *arg, int *out) {
    long n;
    const int low = 0x000000;
    const int high = 0xffffff;

    if (*arg == '\0') {
        output("argument for option '%c' must be non-empty", opt);
        return false;
    }

    if (!strtos(arg, 16, &n)) {
        output("argument for '%c' must be a hexadecimal string", opt);
        return false;
    }

    if (!ranges(n, low, high)) {
        output("argument for '%c' must be between %06x and %06x", opt, low, high);
        return false;
    }

    *out = n;
    return true;
}

static bool parse_key(int opt, const char *arg, key_color kcs[static KEY_COLORS_MAX], size_t *kcs_len) {
    char buf[256];
    char *tmp;
    char *key;
    char *val;
    long n;
    const int low = 0x000000;
    const int high = 0xffffff;
    key_color *kc;

    if (*arg == '\0') {
        output("argument for option '%c' must be non-empty", opt);
        return false;
    }

    strscpy(buf, sizeof(buf), arg);
    tmp = buf;

    key = strsep(&tmp, ":");
    val = strsep(&tmp, ":");

    if (key == NULL || val == NULL || tmp != NULL) {
        output("argument for '%c' must be a colon separated pair of strings", opt);
        return false;
    }

    if (*key == '\0') {
        output("first field of argument for '%c' must be a non-empty string", opt);
        return false;
    }

    if (*val == '\0') {
        output("second field of argument for '%c' must be a non-empty string", opt);
        return false;
    }

    if (!strtos(val, 16, &n)) {
        output("second field of argument for '%c' must be a hexadecimal string", opt);
        return false;
    }

    if (!ranges(n, low, high)) {
        output("second field of argument for '%c' must be between %06x and %06x", opt, low, high);
        return false;
    }

    if (*kcs_len == KEY_COLORS_MAX) {
        output("cannot handle any more key colors");
        return false;
    }

    kc = &kcs[(*kcs_len)++];
    strscpy(kc->key, sizeof(kc->key), key);
    kc->color = n;

    return true;
}

static bool parse_argv(int argc, char **argv) {
    static const char opts[] = ":euD:S:s:m:f:b:d:k:Kh";
    int mode_set_count = 0;
    int opt;

    while ((opt = getopt(argc, argv, opts)) >= 0) {
        if (opt == 'e') {
            ms.program_mode = MODE_ENUMERATE;
            mode_set_count++;
        } else if (opt == 'u') {
            ms.program_mode = MODE_UPDATE;
            mode_set_count++;
        } else if (opt == 'D') {
            if (!parse_str(opt, optarg, &ms.device_id))
                return false;
        } else if (opt == 'S') {
            if (!parse_str(opt, optarg, &ms.settings_path))
                return false;
        } else if (opt == 's') {
            if (!parse_int(opt, optarg, 1, 100, &ms.speed))
                return false;
        } else if (opt == 'm') {
            if (!parse_str2(opt, optarg, ms.mode, sizeof(ms.mode)))
                return false;
        } else if (opt == 'f') {
            if (!parse_rgb24(opt, optarg, &ms.filter))
                return false;
        } else if (opt == 'b') {
            if (!parse_int(opt, optarg, 0, 100, &ms.brightness))
                return false;
        } else if (opt == 'd') {
            if (!parse_int(opt, optarg, 0, 100, &ms.deflection))
                return false;
        } else if (opt == 'k') {
            if (!parse_key(opt, optarg, ms.key_colors, &ms.key_colors_len))
                return false;
        } else if (opt == 'K') {
            ms.default_key_colors = 1;
        } else if (opt == 'h') {
            fprintf(
                stdout,
                "Usage: %s [options]\n"
                "\n"
                "Available options:\n"
                "  -e           enumerate devices\n"
                "  -u           update settings of selected device\n"
                "  -D <string>  serial/usbpath/devpath of selected device\n"
                "  -S <string>  path to settings file\n"
                "  -s <integer> rgb speed\n"
                "  -m <string>  rgb mode\n"
                "  -f <string>  rgb filter\n"
                "  -b <integer> rgb brightness\n"
                "  -d <integer> rgb deflection\n"
                "  -k <string>  rgb key color pair\n"
                "  -K           rgb default key colors\n"
                "  -h           this help text\n",
                ms.program_name
            );
            return false;
        } else if (opt == ':') {
            output("missing argument for option '%c'", optopt);
            return false;
        } else if (opt == '?') {
            output("unknown option '%c'", optopt);
            return false;
        }
    }

    if (mode_set_count == 0) {
        output("no mode requested");
        return false;
    }

    if (mode_set_count > 1) {
        output("only one mode may be requested");
        return false;
    }

    return true;
}

static int enumerate_devices(const device_table_entry *dte, const device_info *di, void *ud) {
    FILE *out = ud;

    if (*di->dev != '\0')
        fprintf(out, "DevPath\t%s\n", di->dev);

    if (*di->usb != '\0')
        fprintf(out, "UsbPath\t%s\n", di->usb);

    if (dte->vid >= 0 && dte->pid >= 0)
        fprintf(out, "DevIds\t%04x:%04x\n", dte->vid, dte->pid);

    if (*di->vendor != '\0')
        fprintf(out, "Vendor\t%s\n", di->vendor);

    if (*di->product != '\0')
        fprintf(out, "Product\t%s\n", di->product);

    if (*di->serial != '\0')
        fprintf(out, "Serial\t%s\n", di->serial);

    fprintf(out, "\n");

    return 0;
}

static int mode_enumerate(void) {
    device_enumerate(enumerate_devices, stdout);
    return EXIT_SUCCESS;
}

static int mode_update(void) {
    device *dev = NULL;
    int ec = EXIT_FAILURE;

    do {
        const device_table_entry *dte;
        device_info di;
        const driver *drv;

        if (!device_find(ms.device_id, &dte, &di)) {
            output("failed to find a usable device");
            break;
        }

        dev = device_open(&di);
        if (dev == NULL) {
            output("failed to open a usable device");
            break;
        }

        drv = driver_find(dte, dev);
        if (drv == NULL) {
            output("failed to find a usable driver");
            break;
        }

        if (!drv->update_mode(dev, ms.mode, ms.speed)) {
            output("failed to update mode");
            break;
        }

        if (!drv->update_filter(dev, ms.filter)) {
            output("failed to update filter");
            break;
        }

        if (!drv->update_brightness(dev, ms.brightness)) {
            output("failed to update brightness");
            break;
        }

        if (!drv->update_key_colors(dev, ms.key_colors, ms.key_colors_len, ms.default_key_colors, ms.deflection)) {
            output("failed to update key colors");
            break;
        }

        ec = EXIT_SUCCESS;
    } while (false);

    device_close(dev);
    return ec;
}

int main(int argc, char **argv) {
    if (!program_init(argv[0]))
        return EXIT_FAILURE;

    if (!parse_argv(argc, argv))
        return EXIT_FAILURE;

    if (!settings_parse(ms.settings_path))
        return EXIT_FAILURE;

    if (ms.program_mode == MODE_ENUMERATE)
        return mode_enumerate();

    if (ms.program_mode == MODE_UPDATE)
        return mode_update();

    output("unhandled mode");
    return EXIT_FAILURE;
}
