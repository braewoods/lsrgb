This project is intended to support the RGB settings for
Light&Salt peripherals. Currently only the first keyboard
version is supported. Other peripherals are known to exist
but it is difficult to acquire them outside of China. Any
assistance in implementing support for these would be
appreciated.

Currently the only thing known to be missing is support
for Apple based operating systems as I have no convenient
access to an installation for development and testing
purposes. Any assistance in implementing support for Apple
would be appreciated.

### Build & Install Procedure
```
meson setup build
meson compile -C build
sudo meson install -C build
```

### Supported Features
 - Device enumeration
 - Device selection
 - RGB Speed
 - RGB Modes
 - RGB Filter
 - RGB Brightness
 - RGB Key Color Deflection
 - RBB Key Colors

### Supported Platforms
 - Windows
 - Linux
 - FreeBSD
 - DragonFlyBSD
 - NetBSD
 - OpenBSD

### Useful Links
 - [Vendor Website](https://www.guangyanjiaohu.com/#/index)
 - [Vendor Software](https://oss.guangyanjiaohu.com/download/Light&Salt_Scene_Assistant.exe)
