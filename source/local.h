// Copyright (c) 2024 James Buren
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#pragma once

#if defined(__linux__)
#define _GNU_SOURCE
#elif defined(__MINGW32__)
#define __MINGW_MSVC_COMPAT_WARNINGS
#define NO_OLDNAMES
#define WINVER _WIN32_WINNT_WIN10
#endif

#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <stdbool.h>
#include <string.h>
#include <time.h>
#include <locale.h>
#include <limits.h>
#include <errno.h>
#include <unistd.h>
#include <dirent.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/file.h>

#if defined(__MINGW32__)
#define tzset(...) _tzset(__VA_ARGS__)
#define memcpy(A,B,C) memcpy_s(A,C,B,C)
#define memccpy(...) _memccpy(__VA_ARGS__)
#else
#include <sys/poll.h>
#include <sys/ioctl.h>
#endif

#include "config.h"
#include "device.h"
#include "driver.h"
#include "hid.h"
#include "settings.h"
#include "extra.h"
#include "utility.h"
#include "main.h"
