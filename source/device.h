// Copyright (c) 2024 James Buren
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#pragma once

enum {
    DEV_PATH_MAX = 256,
    USB_PATH_MAX = 256,
    USB_STRING_MAX = 256,
};

typedef struct device_table_entry {
    int vid;
    int pid;
    int in;
    int mi;
    unsigned int up;
    int irs;
    int ors;
    int frs;
} device_table_entry;

typedef struct device_info {
    char dev[DEV_PATH_MAX];
    char usb[USB_PATH_MAX];
    char vendor[USB_STRING_MAX];
    char product[USB_STRING_MAX];
    char serial[USB_STRING_MAX];
} device_info;

typedef struct device device;

typedef int (*device_enumerator) (const device_table_entry *, const device_info *, void *);

const device_table_entry *device_table_entry_get(int, int, int, unsigned int, int, int, int);
bool device_find(const char *, const device_table_entry **, device_info *);
int device_enumerate(device_enumerator, void *);
device *device_open(const device_info *);
void device_close(device *);
ssize_t device_write(device *, const unsigned char *, size_t);
ssize_t device_read(device *, unsigned char *, size_t, int);
