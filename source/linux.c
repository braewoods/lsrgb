// Copyright (c) 2024 James Buren
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "local.h"
#include <libudev.h>
#include <linux/hidraw.h>

struct device {
    int fd;
};

static bool get_vid_pid_in(struct udev_device *hidraw, int *vid, int *pid, int *in) {
    struct udev_device *iface;
    struct udev_device *dev;
    const char *str;
    long n;

    iface = udev_device_get_parent_with_subsystem_devtype(hidraw, "usb", "usb_interface");
    if (iface == NULL)
        return false;

    dev = udev_device_get_parent_with_subsystem_devtype(hidraw, "usb", "usb_device");
    if (dev == NULL)
        return false;

    str = udev_device_get_sysattr_value(dev, "idVendor");
    if (str != NULL && strtos(str, 16, &n))
        *vid = n;
    else
        *vid = -1;

    str = udev_device_get_sysattr_value(dev, "idProduct");
    if (str != NULL && strtos(str, 16, &n))
        *pid = n;
    else
        *pid = -1;

    str = udev_device_get_sysattr_value(iface, "bInterfaceNumber");
    if (str != NULL && strtos(str, 16, &n))
        *in = n;
    else
        *in = -1;

    return (*vid >= 0 && *pid >= 0 && *in >= 0);
}

static bool get_report_descriptor(struct udev_device *hidraw, struct hidraw_report_descriptor *rd) {
    int fd = -1;
    bool rv = false;

    do {
        char path[PATH_MAX];
        ssize_t n;

        strbuild(
            path,
            sizeof(path),
            udev_device_get_syspath(hidraw),
            "/device/report_descriptor"
        );

        fd = open(path, O_RDONLY | O_CLOEXEC);
        if (fd < 0)
            break;

        n = read(fd, rd->value, sizeof(rd->value));
        if (n < 0)
            break;

        rd->size = n;
        rv = true;
    } while (false);

    if (fd >= 0)
        close(fd);
    return rv;
}

static bool get_device_info(struct udev_device *hidraw, device_info *di) {
    struct udev_device *iface;
    struct udev_device *dev;
    const char *str;

    iface = udev_device_get_parent_with_subsystem_devtype(hidraw, "usb", "usb_interface");
    if (iface == NULL)
        return false;

    dev = udev_device_get_parent_with_subsystem_devtype(hidraw, "usb", "usb_device");
    if (dev == NULL)
        return false;

    memset(di, 0x00, sizeof(*di));

    str = udev_device_get_sysname(hidraw);
    if (str != NULL)
        strscpy(di->dev, sizeof(di->dev), str);

    str = udev_device_get_sysname(iface);
    if (str != NULL)
        strscpy(di->usb, sizeof(di->usb), str);

    str = udev_device_get_sysattr_value(dev, "manufacturer");
    if (str != NULL)
        strscpy(di->vendor, sizeof(di->vendor), str);

    str = udev_device_get_sysattr_value(dev, "product");
    if (str != NULL)
        strscpy(di->product, sizeof(di->product), str);

    str = udev_device_get_sysattr_value(dev, "serial");
    if (str != NULL)
        strscpy(di->serial, sizeof(di->serial), str);

    return true;
}

static const device_table_entry *check_device_table(struct udev_device *child) {
    int vid;
    int pid;
    int in;
    struct hidraw_report_descriptor rd;
    unsigned int up;
    int irs;
    int ors;
    int frs;

    if (!get_vid_pid_in(child, &vid, &pid, &in))
        return NULL;

    if (!get_report_descriptor(child, &rd))
        return NULL;

    hid_report_details(rd.value, rd.size, &up, &irs, &ors, &frs);

    return device_table_entry_get(vid, pid, in, up, irs, ors, frs);
}

int device_enumerate(device_enumerator func, void *data) {
    struct udev *udev = NULL;
    struct udev_enumerate *enumerate = NULL;
    int rv = 0;

    do {
        int res;
        struct udev_list_entry *matches;
        struct udev_list_entry *match;

        udev = udev_new();
        if (udev == NULL)
            break;

        enumerate = udev_enumerate_new(udev);
        if (enumerate == NULL)
            break;

        res = udev_enumerate_add_match_subsystem(enumerate, "hidraw");
        if (res < 0)
            break;

        res = udev_enumerate_scan_devices(enumerate);
        if (res < 0)
            break;

        matches = udev_enumerate_get_list_entry(enumerate);
        udev_list_entry_foreach(match, matches) {
            const char *str;
            struct udev_device *hidraw;
            const device_table_entry *dte;
            device_info di;

            str = udev_list_entry_get_name(match);
            if (str == NULL)
                continue;

            hidraw = udev_device_new_from_syspath(udev, str);
            if (hidraw == NULL)
                continue;

            dte = check_device_table(hidraw);
            res = get_device_info(hidraw, &di);
            udev_device_unref(hidraw);

            if (dte != NULL && res)
                rv = func(dte, &di, data);
            else
                rv = 0;

            if (rv != 0)
                break;
        }
    } while (false);

    udev_enumerate_unref(enumerate);
    udev_unref(udev);
    return rv;
}

device *device_open(const device_info *di) {
    int fd = -1;
    device *dev = NULL;

    do {
        const char *name = di->dev;
        char err[ERR_MAX];
        char path[PATH_MAX];

        strbuild(path, sizeof(path), "/dev/", name);
        fd = open(path, O_RDWR | O_CLOEXEC);

        if (fd < 0) {
            output("%s: %s: %s", "open", errnotostr(err), name);
            break;
        }

        if (flock(fd, LOCK_EX | LOCK_NB) < 0) {
            output("%s: %s: %s", "flock", errnotostr(err), name);
            break;
        }

        // dim already prints an error
        dev = dim0(device, 1);
        if (dev == NULL)
            break;

        dev->fd = fd;
    } while (false);

    if (dev == NULL && fd >= 0)
        close(fd);
    return dev;
}

void device_close(device *dev) {
    if (dev == NULL)
        return;

    close(dev->fd);
    free(dev);
}

ssize_t device_write(device *dev, const unsigned char *buf, size_t size) {
    return write(dev->fd, buf, size);
}

ssize_t device_read(device *dev, unsigned char *buf, size_t size, int to) {
    ssize_t n;

    if (to >= 0) {
        struct pollfd fds;
        int res;

        fds.fd = dev->fd;
        fds.events = POLLIN;

        res = poll(&fds, 1, to);
        if (res <= 0)
            return res;
    }

    *buf = 0x00;
    n = read(dev->fd, buf + 1, size - 1);
    return (n <= 0) ? n : (n + 1);
}
