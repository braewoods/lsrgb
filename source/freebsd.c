// Copyright (c) 2024 James Buren
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "local.h"
#include <sys/sysctl.h>
#include <dev/hid/hidraw.h>
#include <dev/evdev/input.h>
#include <dev/usb/usb_ioctl.h>
#include <dev/usb/usbhid.h>

enum {
    DEVNAME_MAX = 16,
};

enum {
    DEVICE_UNKNOWN = -1,
    DEVICE_HIDRAW,
    DEVICE_UHID,
};

struct device {
    int fd;
    int ws;
};

static int find_device_type(const char *dev) {
    if (is_hidraw(dev))
        return DEVICE_HIDRAW;

    if (is_uhid(dev))
        return DEVICE_UHID;

    return DEVICE_UNKNOWN;
}

static bool hidraw_get_report_descriptor(int fd, unsigned char *data, int *len) {
    struct hidraw_gen_descriptor hgd;

    hgd.hgd_data = data;
    hgd.hgd_maxlen = *len;

    if (ioctl(fd, HIDRAW_GET_REPORT_DESC, &hgd) < 0)
        return false;

    *len = hgd.hgd_actlen;
    return true;
}

static bool uhid_get_report_descriptor(int fd, unsigned char *data, int *len) {
    struct usb_gen_descriptor ugd;

    ugd.ugd_data = data;
    ugd.ugd_maxlen = *len;

    if (ioctl(fd, USB_GET_REPORT_DESC, &ugd) < 0)
        return false;

    *len = ugd.ugd_actlen;
    return true;
}

static bool get_report_descriptor(int dt, int fd, unsigned char *data, int *len) {
    if (dt == DEVICE_HIDRAW)
        return hidraw_get_report_descriptor(fd, data, len);

    if (dt == DEVICE_UHID)
        return uhid_get_report_descriptor(fd, data, len);

    return false;
}

static bool get_uhub_descendant(const char *dev, char buf[DEVNAME_MAX]) {
    char data[DEVNAME_MAX];

    strscpy(data, sizeof(data), dev);

    do {
        size_t n;
        char p1[DEVNAME_MAX];
        char p2[DEVNAME_MAX];
        char name[64];
        size_t len;

        strscpy(buf, DEVNAME_MAX, data);
        n = digcspn(data);
        strscpy(p1, mins(n + 1, sizeof(p1)), data);
        strscpy(p2, sizeof(p2), data + n);

        strbuild(name, sizeof(name), "dev.", p1, ".", p2, ".%parent");
        len = sizeof(data) - 1;

        if (sysctlbyname(name, data, &len, NULL, 0) < 0)
            return false;

        data[len] = '\0';
    } while (!is_uhub(data));

    return true;
}

static bool extract_bus_addr_in(const char *dev, int *bus, int *addr, int *in) {
    char par[DEVNAME_MAX];
    size_t n;
    char p1[DEVNAME_MAX];
    char p2[DEVNAME_MAX];
    char name[64];
    char buf[256];
    size_t len;
    char *ctx;
    char *str;

    if (!get_uhub_descendant(dev, par))
        return false;

    n = digcspn(par);
    strscpy(p1, mins(n + 1, sizeof(p1)), par);
    strscpy(p2, sizeof(p2), par + n);

    strbuild(name, sizeof(name), "dev.", p1, ".", p2, ".%location");
    len = sizeof(buf) - 1;

    if (sysctlbyname(name, buf, &len, NULL, 0) < 0)
        return false;

    buf[len] = '\0';
    ctx = buf;
    *bus = *addr = *in = -1;

    while ((str = strsep(&ctx, " ")) != NULL) {
        char *ctx2 = str;
        char *key = strsep(&ctx2, "=");
        char *val = strsep(&ctx2, "");
        long n;

        if (ctx2 != NULL || key == NULL || val == NULL)
            continue;

        if (strcmp(key, "bus") == 0 && strtos(val, 10, &n))
            *bus = n;
        else if (strcmp(key, "devaddr") == 0 && strtos(val, 10, &n))
            *addr = n;
        else if (strcmp(key, "interface") == 0 && strtos(val, 10, &n))
            *in = n;
    }

    return (*bus != -1 && *addr != -1 && *in != -1);
}

static bool get_device_info(int bus, int addr, struct usb_device_info *udi) {
    char path[PATH_MAX];
    char num1[DIGITS_MAX];
    char num2[DIGITS_MAX];
    int fd;
    int res;

    strbuild(path, sizeof(path), "/dev/ugen", stostr(num1, bus), ".", stostr(num2, addr));

    fd = open(path, O_RDONLY | O_CLOEXEC);
    if (fd < 0)
        return false;

    res = ioctl(fd, USB_GET_DEVICEINFO, udi);
    close(fd);

    return (res >= 0);
}

static bool get_usb_ports(int bus, int addr, int ports[static USB_PORTS_MAX], int *len) {
    char path[PATH_MAX];
    char num1[DIGITS_MAX];
    char num2[DIGITS_MAX];
    int fd;
    struct usb_device_port_path udp;
    int res;

    strbuild(path, sizeof(path), "/dev/ugen", stostr(num1, bus), ".", stostr(num2, addr));

    fd = open(path, O_RDONLY | O_CLOEXEC);
    if (fd < 0)
        return false;

    res = ioctl(fd, USB_GET_DEV_PORT_PATH, &udp);
    close(fd);

    if (res < 0)
        return false;

    for (int i = 0; i < udp.udp_port_level; i++)
        ports[i] = udp.udp_port_no[i];
    *len = udp.udp_port_level;

    return (*len != 0);
}

static void fill_device_info(const char *dev, const int ports[static USB_PORTS_MAX], int len, const struct usb_device_info *udi, int in, device_info *di) {
    memset(di, 0x00, sizeof(*di));
    strscpy(di->dev, sizeof(di->dev), dev);
    format_usb_ports(di->usb, sizeof(di->usb), udi->udi_bus, ports, len, udi->udi_config_no, in);
    strscpy(di->vendor, sizeof(di->vendor), udi->udi_vendor);
    strscpy(di->product, sizeof(di->product), udi->udi_product);
    strscpy(di->serial, sizeof(di->serial), udi->udi_serial);
}

static int handle_device(const char *dev, int dt, int fd, device_enumerator func, void *data) {
    unsigned char rep_data[4096];
    int rep_len = sizeof(rep_data);
    int bus;
    int addr;
    int in;
    struct usb_device_info udi;
    unsigned int up;
    int irs;
    int ors;
    int frs;
    const device_table_entry *dte;
    int ports[USB_PORTS_MAX];
    int len;
    device_info di;

    if (!get_report_descriptor(dt, fd, rep_data, &rep_len))
        return 0;

    if (!extract_bus_addr_in(dev, &bus, &addr, &in))
        return 0;

    if (!get_device_info(bus, addr, &udi))
        return 0;

    hid_report_details(rep_data, rep_len, &up, &irs, &ors, &frs);
    dte = device_table_entry_get(
        udi.udi_vendorNo,
        udi.udi_productNo,
        in,
        up,
        irs,
        ors,
        frs
    );

    if (dte == NULL)
        return 0;
 
    if (!get_usb_ports(udi.udi_bus, udi.udi_addr, ports, &len))
        return 0;

    fill_device_info(dev, ports, len, &udi, in, &di);
    return func(dte, &di, data);
}

int device_enumerate(device_enumerator func, void *data) {
    DIR *dir = NULL;
    int rv = 0;

    do {
        struct dirent *de;

        dir = opendir("/dev");
        if (dir == NULL)
            break;

        while ((de = readdir(dir)) != NULL) {
            const char *dev = de->d_name;
            int dt = find_device_type(dev);
            int fd;

            if (dt == DEVICE_UNKNOWN)
                continue;

            fd = openat(dirfd(dir), dev, O_RDWR | O_CLOEXEC);
            if (fd < 0)
                continue;

            rv = handle_device(dev, dt, fd, func, data);
            close(fd);

            if (rv != 0)
                break;
        }
    } while (false);

    if (dir != NULL)
        closedir(dir);
    return rv;
}

device *device_open(const device_info *di) {
    int fd = -1;
    device *dev = NULL;

    do {
        const char *name = di->dev;
        int dt = find_device_type(name);
        char err[ERR_MAX];
        char path[PATH_MAX];

        if (dt == DEVICE_UNKNOWN) {
            output("unknown device: %s", name);
            break;
        }

        strbuild(path, sizeof(path), "/dev/", name);
        fd = open(path, O_RDWR | O_CLOEXEC);

        if (fd < 0) {
            output("%s: %s: %s", "open", errnotostr(err), name);
            break;
        }

        // dim already prints an error
        dev = dim0(device, 1);
        if (dev == NULL)
            break;

        dev->fd = fd;
        dev->ws = dt;
    } while (false);

    if (dev == NULL && fd >= 0)
        close(fd);
    return dev;
}

void device_close(device *dev) {
    if (dev == NULL)
        return;

    close(dev->fd);
    free(dev);
}

ssize_t device_write(device *dev, const unsigned char *buf, size_t size) {
    int ws = dev->ws;
    ssize_t n = write(dev->fd, buf + ws, size - ws);

    return (n <= 0) ? n : (n + ws);
}

ssize_t device_read(device *dev, unsigned char *buf, size_t size, int to) {
    ssize_t n;

    if (to >= 0) {
        struct pollfd fds;
        int res;

        fds.fd = dev->fd;
        fds.events = POLLIN;

        res = poll(&fds, 1, to);
        if (res <= 0)
            return res;
    }

    *buf = 0x00;
    n = read(dev->fd, buf + 1, size - 1);
    return (n <= 0) ? n : (n + 1);
}
