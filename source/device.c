// Copyright (c) 2024 James Buren
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "local.h"

typedef struct device_find_context {
    const char *key;
    const device_table_entry **dte;
    device_info *di;
} device_find_context;

static int device_find_callback(const device_table_entry *dte, const device_info *di, void *ud) {
    device_find_context *ctx = ud;
    const char *key = ctx->key;

    if (key != NULL && strcmp(key, di->serial) != 0 && strcmp(key, di->usb) != 0 && strcmp(key, di->dev) != 0)
        return 0;

    *ctx->di = *di;
    *ctx->dte = dte;
    return 1;
}

const device_table_entry *device_table_entry_get(int vid, int pid, int in, unsigned int up, int irs, int ors, int frs) {
    static const device_table_entry table[] = {
        {0x0483, 0x5750, 0x01, -1, 0x00010000, 32, 32, 0},
    };
    static const int table_len = array_len(table);

    for (int i = 0; i < table_len; i++) {
        const device_table_entry *dte = &table[i];

        if (dte->vid != vid)
            continue;

        if (dte->pid != pid)
            continue;

        if (in >= 0 && dte->in != in)
            continue;

        if (in < 0 && dte->mi != in)
            continue;

        if (dte->up != up)
            continue;

        if (dte->irs != irs)
            continue;

        if (dte->ors != ors)
            continue;

        if (dte->frs != frs)
            continue;

        return dte;
    }

    return NULL;
}

bool device_find(const char *key, const device_table_entry **dte, device_info *di) {
    device_find_context ctx;

    ctx.key = key;
    ctx.dte = dte;
    ctx.di = di;

    if (device_enumerate(device_find_callback, &ctx) != 0)
        return true;

    return false;
}
