// Copyright (c) 2024 James Buren
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "local.h"
#include <dev/usb/usb.h>
#include <dev/usb/usbhid.h>

struct device {
    int fd;
};

static bool check_report_id(int fd) {
    int rid;

    if (ioctl(fd, USB_GET_REPORT_ID, &rid) < 0)
        return false;

    return (rid == 0x00);
}

static bool get_device_info(int fd, struct usb_device_info *udi) {
    if (ioctl(fd, USB_GET_DEVICEINFO, udi) < 0)
        return false;

    return true;
}

static bool get_report_descriptor(int fd, struct usb_ctl_report_desc *ucrd) {
    if (ioctl(fd, USB_GET_REPORT_DESC, ucrd) < 0)
        return false;

    return true;
}

static void fill_device_info(const char *dev, const int ports[static USB_PORTS_MAX], int len, const struct usb_device_info *udi, int in, device_info *di) {
    memset(di, 0x00, sizeof(*di));
    strscpy(di->dev, sizeof(di->dev), dev);
    format_usb_ports(di->usb, sizeof(di->usb), udi->udi_bus, ports, len, udi->udi_config, in);
    strscpy(di->vendor, sizeof(di->vendor), udi->udi_vendor);
    strscpy(di->product, sizeof(di->product), udi->udi_product);
    strscpy(di->serial, sizeof(di->serial), udi->udi_serial);
}

static int handle_uhid(const char *dev, int fd, int *bus, int *addr, int *in, device_enumerator func, void *data) {
    struct usb_device_info udi;
    struct usb_ctl_report_desc ucrd;
    unsigned int up;
    int irs;
    int ors;
    int frs;
    const device_table_entry *dte;
    int ports[USB_PORTS_MAX];
    device_info di;

    if (!check_report_id(fd))
        return 0;

    if (!get_device_info(fd, &udi))
        return 0;

    if (!get_report_descriptor(fd, &ucrd))
        return 0;

    if (*bus != udi.udi_bus || *addr != udi.udi_addr) {
        *bus = udi.udi_bus;
        *addr = udi.udi_addr;
        *in = 1;
    } else {
        (*in)++;
    }

    hid_report_details(ucrd.ucrd_data, ucrd.ucrd_size, &up, &irs, &ors, &frs);
    dte = device_table_entry_get(
        udi.udi_vendorNo,
        udi.udi_productNo,
        -*in,
        up,
        irs,
        ors,
        frs
    );

    if (dte == NULL)
        return 0;

    ports[0] = udi.udi_port;
    fill_device_info(dev, ports, 1, &udi, *in, &di);
    return func(dte, &di, data);
}

int device_enumerate(device_enumerator func, void *data) {
    DIR *dir = NULL;
    int rv = 0;

    do {
        struct dirent *de;
        int bus = -1;
        int addr = -1;
        int in = -1;

        dir = opendir("/dev");
        if (dir == NULL)
            break;

        while ((de = readdir(dir)) != NULL) {
            const char *dev = de->d_name;
            int fd;

            if (!is_uhid(dev))
                continue;

            fd = openat(dirfd(dir), dev, O_RDWR | O_CLOEXEC);
            if (fd < 0)
                continue;

            rv = handle_uhid(dev, fd, &bus, &addr, &in, func, data);
            close(fd);

            if (rv != 0)
                break;
        }
    } while (false);

    if (dir != NULL)
        closedir(dir);
    return rv;
}

device *device_open(const device_info *di) {
    int fd = -1;
    device *dev = NULL;

    do {
        const char *name = di->dev;
        char err[ERR_MAX];
        char path[PATH_MAX];

        strbuild(path, sizeof(path), "/dev/", name);
        fd = open(path, O_RDWR | O_CLOEXEC);

        if (fd < 0) {
            output("%s: %s: %s", "open", errnotostr(err), name);
            break;
        }

        // dim already prints an error
        dev = dim0(device, 1);
        if (dev == NULL)
            break;

        dev->fd = fd;
    } while (false);

    if (dev == NULL && fd >= 0)
        close(fd);
    return dev;
}

void device_close(device *dev) {
    if (dev == NULL)
        return;

    close(dev->fd);
    free(dev);
}

ssize_t device_write(device *dev, const unsigned char *buf, size_t size) {
    ssize_t n = write(dev->fd, buf + 1, size - 1);

    return (n <= 0) ? n : (n + 1);
}

ssize_t device_read(device *dev, unsigned char *buf, size_t size, int to) {
    ssize_t n;

    if (to >= 0) {
        struct pollfd fds;
        int res;

        fds.fd = dev->fd;
        fds.events = POLLIN;

        res = poll(&fds, 1, to);
        if (res <= 0)
            return res;
    }

    *buf = 0x00;
    n = read(dev->fd, buf + 1, size - 1);
    return (n <= 0) ? n : (n + 1);
}
