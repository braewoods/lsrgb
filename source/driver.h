// Copyright (c) 2024 James Buren
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#pragma once

enum {
    DRIVER_V1_IRS = 32,
    DRIVER_V1_ORS = 32,
    DRIVER_V1_FRS = 0,
};

typedef struct key_color {
    char key[28];
    int color;
} key_color;

typedef struct driver {
    bool (*update_mode) (device *, const char *, int);
    bool (*update_filter) (device *, int);
    bool (*update_brightness) (device *, int);
    bool (*update_key_colors) (device *, const key_color *, size_t, int, int);
} driver;

const driver *driver_find(const device_table_entry *dte, device *dev);
extern const driver driver_keyboard_v1;
