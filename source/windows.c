// Copyright (c) 2024 James Buren
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "local.h"
#include <wtypesbase.h>
#include <hidsdi.h>
#include <cfgmgr32.h>

#if !defined(_UCRT)
#error "Only UCRT toolchains are supported."
#endif

struct device {
    HANDLE handle;
    OVERLAPPED read;
    OVERLAPPED write;
};

static int winerr_to_errno(void) {
    DWORD err = GetLastError();

    if ((err & 0xffff0000) == 0x80070000) {
        err &= 0x0000ffff;
    }

    if (err >= 10000 && err < 12000) {
        switch (err) {
            case WSAEINTR:
            case WSAEBADF:
            case WSAEACCES:
            case WSAEFAULT:
            case WSAEINVAL:
            case WSAEMFILE:
                return err - 10000;
            default:
                return err;
        }
    }

    switch (err) {
        case ERROR_FILE_NOT_FOUND:
        case ERROR_PATH_NOT_FOUND:
        case ERROR_INVALID_DRIVE:
        case ERROR_NO_MORE_FILES:
        case ERROR_BAD_NETPATH:
        case ERROR_BAD_NET_NAME:
        case ERROR_BAD_PATHNAME:
        case ERROR_FILENAME_EXCED_RANGE:
            return ENOENT;

        case ERROR_BAD_ENVIRONMENT:
            return E2BIG;

        case ERROR_BAD_FORMAT:
        case ERROR_INVALID_STARTING_CODESEG:
        case ERROR_INVALID_STACKSEG:
        case ERROR_INVALID_MODULETYPE:
        case ERROR_INVALID_EXE_SIGNATURE:
        case ERROR_EXE_MARKED_INVALID:
        case ERROR_BAD_EXE_FORMAT:
        case ERROR_ITERATED_DATA_EXCEEDS_64k:
        case ERROR_INVALID_MINALLOCSIZE:
        case ERROR_DYNLINK_FROM_INVALID_RING:
        case ERROR_IOPL_NOT_ENABLED:
        case ERROR_INVALID_SEGDPL:
        case ERROR_AUTODATASEG_EXCEEDS_64k:
        case ERROR_RING2SEG_MUST_BE_MOVABLE:
        case ERROR_RELOC_CHAIN_XEEDS_SEGLIM:
        case ERROR_INFLOOP_IN_RELOC_CHAIN:
            return ENOEXEC;

        case ERROR_INVALID_HANDLE:
        case ERROR_INVALID_TARGET_HANDLE:
        case ERROR_DIRECT_ACCESS_HANDLE:
            return EBADF;

        case ERROR_WAIT_NO_CHILDREN:
        case ERROR_CHILD_NOT_COMPLETE:
            return ECHILD;

        case ERROR_NO_PROC_SLOTS:
        case ERROR_MAX_THRDS_REACHED:
        case ERROR_NESTING_NOT_ALLOWED:
            return EAGAIN;

        case ERROR_ARENA_TRASHED:
        case ERROR_NOT_ENOUGH_MEMORY:
        case ERROR_INVALID_BLOCK:
        case ERROR_NOT_ENOUGH_QUOTA:
            return ENOMEM;

        case ERROR_ACCESS_DENIED:
        case ERROR_CURRENT_DIRECTORY:
        case ERROR_WRITE_PROTECT:
        case ERROR_BAD_UNIT:
        case ERROR_NOT_READY:
        case ERROR_BAD_COMMAND:
        case ERROR_CRC:
        case ERROR_BAD_LENGTH:
        case ERROR_SEEK:
        case ERROR_NOT_DOS_DISK:
        case ERROR_SECTOR_NOT_FOUND:
        case ERROR_OUT_OF_PAPER:
        case ERROR_WRITE_FAULT:
        case ERROR_READ_FAULT:
        case ERROR_GEN_FAILURE:
        case ERROR_SHARING_VIOLATION:
        case ERROR_LOCK_VIOLATION:
        case ERROR_WRONG_DISK:
        case ERROR_SHARING_BUFFER_EXCEEDED:
        case ERROR_NETWORK_ACCESS_DENIED:
        case ERROR_CANNOT_MAKE:
        case ERROR_FAIL_I24:
        case ERROR_DRIVE_LOCKED:
        case ERROR_SEEK_ON_DEVICE:
        case ERROR_NOT_LOCKED:
        case ERROR_LOCK_FAILED:
        case 35:
            return EACCES;

        case ERROR_FILE_EXISTS:
        case ERROR_ALREADY_EXISTS:
            return EEXIST;

        case ERROR_NOT_SAME_DEVICE:
            return EXDEV;

        case ERROR_DIRECTORY:
            return ENOTDIR;

        case ERROR_TOO_MANY_OPEN_FILES:
            return EMFILE;

        case ERROR_DISK_FULL:
            return ENOSPC;

        case ERROR_BROKEN_PIPE:
        case ERROR_NO_DATA:
            return EPIPE;

        case ERROR_DIR_NOT_EMPTY:
            return ENOTEMPTY;

        case ERROR_NO_UNICODE_TRANSLATION:
            return EILSEQ;

        case ERROR_INVALID_FUNCTION:
        case ERROR_INVALID_ACCESS:
        case ERROR_INVALID_DATA:
        case ERROR_INVALID_PARAMETER:
        case ERROR_NEGATIVE_SEEK:
        default:
            return EINVAL;
    }
}

static const char *winerrtostr(char err[static ERR_MAX]) {
    return errtostr(err, winerr_to_errno());
}

static void winerr_set_errno(void) {
    errno = winerr_to_errno();
}

static char *get_device_interface_list(void) {
    GUID guid;
    char *dil = NULL;
    bool ok = false;

    HidD_GetHidGuid(&guid);

    while (true) {
        ULONG len;
        CONFIGRET cr;

        cr = CM_Get_Device_Interface_List_Size(
            &len,
            &guid,
            NULL,
            CM_GET_DEVICE_INTERFACE_LIST_PRESENT
        );

        if (cr != CR_SUCCESS)
            break;

        dil = realloc(dil, len);
        if (dil == NULL)
            break;

        cr = CM_Get_Device_Interface_List(
            &guid,
            NULL,
            dil,
            len,
            CM_GET_DEVICE_INTERFACE_LIST_PRESENT
        );

        if (cr == CR_BUFFER_SMALL)
            continue;

        if (cr != CR_SUCCESS)
            break;

        ok = true;
        break;
    }

    if (!ok) {
        free(dil);
        dil = NULL;
    }
    return dil;
}

static bool extract_vid_pid_in(const char *dev, int *vid, int *pid, int *in) {
    char buf[DEV_PATH_MAX];
    char *ctx;
    char *str;

    *vid = -1;
    *pid = -1;
    *in = 0;

    strscpy(buf, sizeof(buf), dev);
    ctx = buf;

    while ((str = strsep(&ctx, "#&")) != NULL) {
        char *ctx2 = str;
        char *key = strsep(&ctx2, "_");
        char *val = strsep(&ctx2, "");
        long n;

        if (ctx2 != NULL || key == NULL || val == NULL)
            continue;

        if (strcmp(key, "VID") == 0 && strtos(val, 16, &n))
            *vid = n;
        else if (strcmp(key, "PID") == 0 && strtos(val, 16, &n))
            *pid = n;
        else if (strcmp(key, "MI") == 0 && strtos(val, 16, &n))
            *in = n;
    }

    return (*vid != -1 && *pid != -1);
}

static bool get_caps(HANDLE handle, PHIDP_CAPS caps) {
    PHIDP_PREPARSED_DATA data;
    NTSTATUS res;

    if (!HidD_GetPreparsedData(handle, &data))
        return false;

    res = HidP_GetCaps(data, caps);
    HidD_FreePreparsedData(data);

    return (res == HIDP_STATUS_SUCCESS);
}

static bool get_vendor(HANDLE handle, char *vendor, size_t size) {
    wchar_t buf[USB_STRING_MAX];
    int res;

    if (!HidD_GetManufacturerString(handle, buf, sizeof(buf)))
        return false;

    res = WideCharToMultiByte(
        CP_UTF8,
        WC_ERR_INVALID_CHARS | WC_NO_BEST_FIT_CHARS,
        buf,
        -1,
        vendor,
        size,
        NULL,
        NULL
    );

    return (res != 0);
}

static bool get_product(HANDLE handle, char *product, size_t size) {
    wchar_t buf[USB_STRING_MAX];
    int res;

    if (!HidD_GetProductString(handle, buf, sizeof(buf)))
        return false;

    res = WideCharToMultiByte(
        CP_UTF8,
        WC_ERR_INVALID_CHARS | WC_NO_BEST_FIT_CHARS,
        buf,
        -1,
        product,
        size,
        NULL,
        NULL
    );

    return (res != 0);
}

static bool get_serial(HANDLE handle, char *serial, size_t size) {
    wchar_t buf[USB_STRING_MAX];
    int res;

    if (!HidD_GetSerialNumberString(handle, buf, sizeof(buf)))
        return false;

    res = WideCharToMultiByte(
        CP_UTF8,
        WC_ERR_INVALID_CHARS | WC_NO_BEST_FIT_CHARS,
        buf,
        -1,
        serial,
        size,
        NULL,
        NULL
    );

    return (res != 0);
}

static void fill_device_info(const char *dev, HANDLE handle, device_info *di) {
    memset(di, 0x00, sizeof(*di));
    strscpy(di->dev, sizeof(di->dev), dev);
    get_vendor(handle, di->vendor, sizeof(di->vendor));
    get_product(handle, di->product, sizeof(di->product));
    get_serial(handle, di->serial, sizeof(di->serial));
}

static int handle_hidd(const char *dev, HANDLE handle, device_enumerator func, void *data) {
    int vid;
    int pid;
    int in;
    HIDP_CAPS caps;
    unsigned int up;
    int irs;
    int ors;
    int frs;
    const device_table_entry *dte;
    device_info di;

    if (!extract_vid_pid_in(dev, &vid, &pid, &in))
        return 0;

    if (!get_caps(handle, &caps))
        return 0;

    up = (caps.UsagePage << 16) | (caps.Usage << 0);

    irs = caps.InputReportByteLength;
    if (irs > 0)
        irs--;

    ors = caps.OutputReportByteLength;
    if (ors > 0)
        ors--;

    frs = caps.FeatureReportByteLength;
    if (frs > 0)
        frs--;

    dte = device_table_entry_get(vid, pid, in, up, irs, ors, frs);
    if (dte == NULL)
        return 0;

    fill_device_info(dev, handle, &di);
    return func(dte, &di, data);
}

int device_enumerate(device_enumerator func, void *data) {
    char *dil = NULL;
    int rv = 0;

    do {
        dil = get_device_interface_list();
        if (dil == NULL)
            break;

        for (const char *dev = dil; *dev != '\0'; dev += strlen(dev) + 1) {
            HANDLE handle;

            handle = CreateFile(
                dev,
                GENERIC_READ | GENERIC_WRITE,
                FILE_SHARE_READ | FILE_SHARE_WRITE,
                NULL,
                OPEN_EXISTING,
                FILE_ATTRIBUTE_NORMAL | FILE_FLAG_OVERLAPPED,
                NULL
            );

            if (handle == INVALID_HANDLE_VALUE)
                continue;

            rv = handle_hidd(dev, handle, func, data);
            CloseHandle(handle);

            if (rv != 0)
                break;
        }
    } while (false);

    free(dil);
    return rv;
}

device *device_open(const device_info *di) {
    HANDLE handle = INVALID_HANDLE_VALUE;
    HANDLE read = NULL;
    HANDLE write = NULL;
    device *dev = NULL;

    do {
        char err[ERR_MAX];
        const char *name = di->dev;

        handle = CreateFile(
            name,
            GENERIC_READ | GENERIC_WRITE,
            0,
            NULL,
            OPEN_EXISTING,
            FILE_ATTRIBUTE_NORMAL | FILE_FLAG_OVERLAPPED,
            NULL
        );

        if (handle == INVALID_HANDLE_VALUE) {
            output("%s: %s: %s", "CreateFile", winerrtostr(err), name);
            break;
        }

        read = CreateEvent(NULL, FALSE, FALSE, NULL);
        if (read == NULL) {
            output("%s: %s: %s", "CreateEvent", winerrtostr(err), name);
            break;
        }

        write = CreateEvent(NULL, FALSE, FALSE, NULL);
        if (write == NULL) {
            output("%s: %s: %s", "CreateEvent", winerrtostr(err), name);
            break;
        }

        // dim already prints an error
        dev = dim0(device, 1);
        if (dev == NULL)
            break;

        dev->handle = handle;
        dev->read.hEvent = read;
        dev->write.hEvent = write;
    } while (false);

    if (dev == NULL) {
        if (handle != INVALID_HANDLE_VALUE)
            CloseHandle(handle);
        if (read != NULL)
            CloseHandle(read);
        if (write != NULL)
            CloseHandle(write);
    }
    return dev;
}

void device_close(device *dev) {
    if (dev == NULL)
        return;

    CancelIo(dev->handle);
    CloseHandle(dev->read.hEvent);
    CloseHandle(dev->write.hEvent);
    CloseHandle(dev->handle);
    free(dev);
}

ssize_t device_write(device *dev, const unsigned char *buf, size_t size) {
    ssize_t rv;

    do {
        DWORD len;

        if (WriteFile(dev->handle, buf, size, &len, &dev->write)) {
            rv = len;
            break;
        }

        if (GetLastError() != ERROR_IO_PENDING) {
            rv = -1;
            break;
        }

        if (!GetOverlappedResult(dev->handle, &dev->write, &len, TRUE)) {
            rv = -1;
            break;
        }

        rv = len;
    } while (false);

    if (rv <= 0)
        CancelIo(dev->handle);
    if (rv == -1)
        winerr_set_errno();
    return rv;
}

ssize_t device_read(device *dev, unsigned char *buf, size_t size, int to) {
    ssize_t rv;

    do {
        DWORD len;

        if (ReadFile(dev->handle, buf, size, &len, &dev->read)) {
            rv = len;
            break;
        }

        if (GetLastError() != ERROR_IO_PENDING) {
            rv = -1;
            break;
        }

        if (to >= 0) {
            DWORD res = WaitForSingleObject(dev->read.hEvent, to);

            if (res == WAIT_FAILED) {
                rv = -1;
                break;
            }

            if (res == WAIT_TIMEOUT) {
                rv = 0;
                break;
            }
        }

        if (!GetOverlappedResult(dev->handle, &dev->read, &len, TRUE)) {
            rv = -1;
            break;
        }

        rv = len;
    } while (false);

    if (rv <= 0)
        CancelIo(dev->handle);
    if (rv == -1)
        winerr_set_errno();
    return rv;
}
