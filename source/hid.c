// Copyright (c) 2024 James Buren
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "local.h"
#include "usbhid.h"

static unsigned int hid_usage_page(const report_desc_t *rd) {
    hid_data_t d;
    hid_item_t h;
    unsigned int up;

    d = hid_start_parse(rd, 1 << hid_collection, NO_REPORT_ID);
    memset(&h, 0x00, sizeof(h));
    up = 0xffffffff;

    while (hid_get_item(d, &h)) {
        if (h.kind == hid_collection) {
            up = h.usage;
            break;
        }
    }

    hid_end_parse(d);
    return up;
}

void hid_report_details(const unsigned char *data, size_t size, unsigned int *up, int *irs, int *ors, int *frs) {
    report_desc_t rd;

    rd.size = size;
    memcpy(rd.data, data, size);

    *up = hid_usage_page(&rd);
    *irs = hid_report_size(&rd, hid_input, NO_REPORT_ID);
    *ors = hid_report_size(&rd, hid_output, NO_REPORT_ID);
    *frs = hid_report_size(&rd, hid_feature, NO_REPORT_ID);
}
